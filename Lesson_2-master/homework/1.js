var btn, tab, wrapBtn, hideBtn;
btn = document.querySelectorAll('.showButton');
tab = document.querySelectorAll('.tab');
wrapBtn = document.getElementById('buttonContainer');
hideBtn = document.createElement('button');
hideBtn.innerHTML = 'Hide them all! \\o/';
wrapBtn.appendChild(hideBtn);

btn.forEach(function (item, i) {
	btn[i].onclick = function (e) {
		e.preventDefault();
		tab.forEach(function (item, i) {
			console.log(tab[i]);
			if (tab[i].dataset.tab !== e.target.dataset.tab) {
				tab[i].classList.remove('active');
			} else {
				tab[i].classList.add('active');

			}
		});

	}
});

hideBtn.onclick = function () {
	tab.forEach(function (item) {
		item.classList.remove('active');
	});
};


/*

 Задание 1.

 Написать скрипт который будет будет переключать вкладки по нажатию
 на кнопки в хедере.

 Главное условие - изменять файл HTML нельзя.

 Алгоритм:
 1. Выбрать каждую кнопку в шапке
 + бонус выбрать одним селектором

 2. Повесить кнопку онклик
 button1.onclick = function(event) {

 }
 + бонус: один обработчик на все три кнопки

 3. Написать функцию которая выбирает соответствующую вкладку
 и добавляет к ней класс active

 4. Написать функцию hideAllTabs которая прячет все вкладки.
 Удаляя класс active со всех вкладок

 Методы для работы:

 getElementById
 querySelector
 classList
 classList.add
 forEach
 onclick

 element.onclick = function(event) {
 // do stuff ...
 }

 */
