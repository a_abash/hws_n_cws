/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/

let Train = {
  name: 'bla',
  speed: '12',
  passengers: '1',
  drive: function () {
    console.log('Поезд ' + this.name + ' везет ' + this.passengers + ' со скоростью ' + this.speed);
  },
  stop: function () {
    console.log('Поезд ' + this.name + ' остановился. Скорость ' + this.speed);
  },
  takePass: function (num) {
    this.passengers += num;
    console.log('Подобрать пассажиров ' + this.passengers);
  }
};

Train.drive();
Train.stop();
Train.takePass(1);