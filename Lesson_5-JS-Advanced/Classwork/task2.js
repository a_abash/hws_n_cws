/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент, второй попадает в него через метод .call(obj)
    1.2 Ф-я не принимает никаких аргументов, а необходимые настройки полностью передаются через bind
    1.3 Ф-я принимает фразу для заголовка, обьект с настройками передаем через .apply();

*/


let color = {
  background: 'purple',
  color: 'lightgrey'
};

// function op(back) {
//   let b = document.createElement('h1');
//   document.body.appendChild(b);
//   b.innerText = 'qwerty';
//   document.body.style.background = back;
//   document.body.style.color = this.color;
// }

function op2() {
  let b = document.createElement('h1');
  document.body.appendChild(b);
  b.innerText = 'qwerty';
  document.body.style.background = this.background;
  document.body.style.color = this.color;
}

// function op3(text) {
//   let b = document.createElement('h1');
//   document.body.appendChild(b);
//   b.innerText = text;
//   document.body.style.background = this.background;
//   document.body.style.color = this.color;
// }


// op.call(color, 'orange');

let addOp = op2.bind(color);
addOp();

// op3.apply(color, ['text']);
