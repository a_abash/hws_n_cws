/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }

*/

function Dog(name, breed) {
  this.name = name;
  this.breed = breed;
  this.action = null;
  
  this.something = function () {
    if (this.action == null) {
      this.action = 'run';
      console.log(this.action);
    } else if (this.action === 'run') {
      this.action = 'eat';
      console.log(this.action);
    }
  };
  
  this.bla = function () {
    for (let key in this) {
      console.log(key, this[key]);
    }
  };
}

let newDog = new Dog('jora', 'zalupa');

newDog.something();
newDog.something();
newDog.bla();