/*

 Задание "Шифр цезаря":

 https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

 Написать функцию, которая будет принимать в себя слово и количество
 симовлов на которые нужно сделать сдвиг внутри.

 Написать функцию дешефратор которая вернет слово в изначальный вид.

 Сделать статичные функции используя bind и метод частичного
 вызова функции (каррирования), которая будет создавать и дешефровать
 слова с статично забитым шагом от одного до 5.

 Например:
 encryptCesar('Word', 3);
 encryptCesar1(...)
 ...
 encryptCesar5(...)

 decryptCesar1('Sdwq', 3);
 decryptCesar1(...)
 ...
 decryptCesar5(...)

 */
window.onload = function () {
  let text = document.getElementById('text');
  text.setAttribute('onchange', 'myFunc()');
};

function myFunc() {
  let textVal = text.value;
  //change AMOUNT THERE
  let amount = 3;
  //change AMOUNT THERE

  let encryptGo = encrypt.bind(null, textVal, amount);
  encryptGo();
  let decryptGo = decrypt.bind(null, textVal, amount);
  decryptGo();

  function encrypt(word1, amount1) {
    let assemble1 = '';

    for (let i = 0; i < word1.length; i++) {
      // console.log(i);
      let letters1 = word1.charCodeAt(i);
      // console.log(letters1);
      if(letters1 >= 97 && letters1 <= 122) {
        assemble1 += String.fromCharCode((letters1 - 97 + amount1) % 26 + 97);
      } else if(letters1 >= 65 && letters1 <= 90) {
        assemble1 += String.fromCharCode((letters1 - 65 + amount1) % 26 + 65);
      } else {
        assemble1 += String.fromCharCode(letters1 + amount1);
      }
    }
    alert('Encrypt - ' + assemble1);
    console.log(assemble1);
  }

  function decrypt(word2) {
    let assemble2 = '';

    for (let i = 0; i < word2.length; i++) {
      // console.log(i);
      let letters2 = word2.charCodeAt(i);
      // console.log(letters2);
      assemble2 += String.fromCharCode(letters2);
    }
    alert('Decrypt - ' + assemble2);
    console.log(assemble2);

  }
}


