/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

*/
let obj = {
  url: 'https://kooledge.com/assets/default_medium_avatar-57d58da4fc778fbd688dcbc4cbc47e14ac79839a9801187e42a796cbd6569847.png',
  incLikes: function () {
    this.likes++;
  }
};

let myArray = [];

 function Comment(name, text, avatarUrl) {
    Object.setPrototypeOf(this, obj);
    this.name = name;
    this.text = text;
    if (avatarUrl === undefined) {
      this.avatarUrl = this.url;
    } else {
    this.avatarUrl = avatarUrl;
    }
    this.likes = 1;
    myArray.push(this);
 }
 
 let comment1 = new Comment('Vasya', 'Hello world');
 let comment2 = new Comment('Tanya', 'Півник петя, ататата!', 'https://cdn2.iconfinder.com/data/icons/rcons-user/32/male-shadow-circle-512.png');
 let comment3 = new Comment('Artem', 'Стапамба, шц!', 'https://cdn2.iconfinder.com/data/icons/rcons-user/32/male-shadow-circle-512.png');
 let comment4 = new Comment('Max', 'Буууууу!', 'https://cdn2.iconfinder.com/data/icons/rcons-user/32/male-shadow-circle-512.png');
 
 function ShowComments(array) {
   let feed = document.querySelector('#CommentsFeed');
   array.forEach(function (item) {
     let divM = document.createElement('div');
         divM.id = 'op';
     feed.appendChild(divM);
     
     let div = createTextNode('div', item.name);
         divM.appendChild(div);
     let p = createTextNode('p', item.text);
         divM.appendChild(p);
     let img = createTextNode ('img', '');
         divM.appendChild(img);
         img.setAttribute('src', item.avatarUrl);
     let likeBtn = createTextNode('input');
         likeBtn.setAttribute('type', 'button');
         likeBtn.setAttribute('value', 'like');
         divM.appendChild(likeBtn);
         
     let addLikes = item.incLikes.bind(item);
     likeBtn.addEventListener('click', function () {
         likeBtn.setAttribute('value', 'like ' + item.likes);
       addLikes();
     });
   })
 }
 
 function createTextNode(elem, name) {
   let el = document.createElement(elem);
   el.textContent = name;
   return el;
 }
 
 let show = new ShowComments(myArray);
 
 
